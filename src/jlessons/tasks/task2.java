package jlessons.tasks;

import java.util.Scanner;

public class task2 {
    public static void main(String[] args) {
        int point;
        int sum = 0;
        float AverageValue = 0;
        Scanner input = new Scanner(System.in);
        System.out.println("Enter points(Enter -1 to and entering): ");
        do {
            point = input.nextInt();
            if (point != -1) {
                sum += point;
                AverageValue++;
            }
        } while (point != -1);
        AverageValue = sum / AverageValue;
        System.out.println("Sum of all points: " + sum);
        System.out.println("Average value: " + AverageValue);
    }
}
