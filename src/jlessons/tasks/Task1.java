package jlessons.tasks;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter three sides of triangle: ");
        int a = input.nextInt();
        int b = input.nextInt();
        int c = input.nextInt();
        int biggest = Math.max(Math.max(a, b), c);
        if (biggest < a + b + c - biggest)
            System.out.println("You can build a triangle with sides: " + a + "," + b + "," + c);
        else
            System.out.println("You can`t build a triangle with sides: " + a + "," + b + "," + c);
    }
}
