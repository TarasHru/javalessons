package jlessons.tasks;

import java.util.Scanner;

public class task3_2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        long number;
        do {
            System.out.println("Enter five sins number: ");
            number = input.nextLong();
        } while (number < 10000 || number > 99999);
        if (number % 10 == number / 10000)
            if ((number % 100 - number % 10) / 10 == number / 1000 - (number / 10000) * 10)
                System.out.println("Number " + number + " is palindrome");
            else
                System.out.println("Number " + number + " isn`t palindrome");

    }
}
