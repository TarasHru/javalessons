package jlessons.tasks.task4;

public class task4 {
    private static int strange = 0;
    private static int normal = 0;

    public static void main(String[] args) {
        System.out.println("Running main");

       String[] numberTypes = testStrangeNumbers();

        countNumTypes(numberTypes);

    }

    static String checkStrangeness(int num) {
        if (num % 2 != 0||(num > 6 && num < 20))
            return ("Strange");
        else if (num % 2 == 0 && ((num > 2 && num < 5) || num > 20))
            return ("Normal");
        return null;
    }

    private static void countNumTypes(String[] types) {
        for (int i = 0; i < 6; i++) {
            switch (types[i]){
                case ("Strange"):
                    strange++;
                    break;
                case ("Normal"):
                    normal++;
                    break;
            }
        }
    }


    private static String[] testStrangeNumbers() {

        String[] numberTypes = new String[6];
        numberTypes[0] = checkStrangeness(3);
        numberTypes[1] = checkStrangeness(1);
        numberTypes[2] = checkStrangeness(4);
        numberTypes[3] = checkStrangeness(22);
        numberTypes[4] = checkStrangeness(18);
        numberTypes[5] = checkStrangeness(26);

        return numberTypes;
    }


    static int getStrange() {
        return strange;
    }

    static int getNormal() {
        return normal;
    }
}
