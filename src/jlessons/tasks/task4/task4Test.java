package jlessons.tasks.task4;

//import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class task4Test {
    @Test
    public void testStrangenes0() {
        //3
        assertEquals("3 Should be Strange", "Strange", task4.checkStrangeness(3));
    }

    @Test
    public void testStrangenes1() {
        //24
        assertEquals("24 Should be Normal", "Normal", task4.checkStrangeness(24));
    }


    @Test
    public void testStrangenes2() {
        //15
        assertEquals("15 Should be Strange", "Strange", task4.checkStrangeness(15));
    }

    @Test
    public void testStrangenes3() {
        //29
        assertEquals("29 Should be Strange", "Strange", task4.checkStrangeness(29));
    }

    @Test
    public void testStrangenes4() {
        //5
        assertEquals("5 Should be Strange", "Strange", task4.checkStrangeness(5));
    }

    @Test
    public void testStrangenes5() {
        //100
        assertEquals("100 Should be Normal", "Normal", task4.checkStrangeness(100));
    }

    @BeforeClass
    public static void callMain() {
        task4.main(new String[0]);
    }

    @Test
    public void testStrangeNum() {
        assertEquals("Expected 3 Strange numbers", 3, task4.getStrange());
    }

    @Test
    public void testNormalNum() {
        assertEquals("Expected 3 Normal numbers", 3, task4.getNormal());
    }
}