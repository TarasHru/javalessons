package jlessons.tasks;

import java.io.IOException;
import java.util.Scanner;

public class task0 {
    public static void main(String[] args) throws IOException {

        Scanner input = new Scanner(System.in);
        System.out.println("Enter number of iterations:");
        int IterationNumber = input.nextInt();
        while (IterationNumber != 0) {
            IterationNumber--;
            System.out.println("Enter the group of three numbers: ");
            int a = input.nextInt();
            int b = input.nextInt();
            int n = input.nextInt();
            System.out.println("Output: ");
            for (int i = 0; i < n; i++) {
                a += Math.pow(2 ,i) * b;
                System.out.println(a + " ");
            }
        }
    }
}