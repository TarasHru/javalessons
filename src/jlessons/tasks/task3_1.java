package jlessons.tasks;

import java.util.Scanner;

public class task3_1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        long number;
        do {
            System.out.println("Enter five signs number: ");
            number = input.nextLong();
        } while (number < 10000 || number > 99999);
        String snumber = Long.toString(number);
        if (snumber.substring(0, 1).equals(snumber.substring(4, 5)))
            if (snumber.substring(1, 2).equals(snumber.substring(3, 4)))
                System.out.println("Number " + number + " is palindrome");
            else
                System.out.println("Number " + number + " isn`t palindrome");
    }
}
